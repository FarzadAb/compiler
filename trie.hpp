#ifndef __SCANNER_TRIE__
#define __SCANNER_TRIE__ 1

enum State {
    STATE_NOTHING_YET,
    STATE_OK,
    STATE_OK_CHARBACK,
    STATE_ERROR
};

#define TokenState pair<TokenType, State>
#define tType first
#define tState second

#include <iostream>
#include <fstream>
#include <map>
#include <set>
#include "token.hpp"
using namespace std;

struct Node{
    map<char,Node*> child;
    TokenType kind;
    bool markedForDeath;
    bool recursiveDelete();
    Node();
};

struct Trie {
    Node* head;
    set<char> legalChars,whiteSpace;
    Node* nowPT; // used for iteration on DFA
    bool lastTokenSPOperator;

    TokenState nextStep(char c);

    Node* addString(string in, TokenType kind);
    void  addInteger(); // add nodes related to Integer Detection
    void  addIdentifier(); // add nodes related to Indentifier Detection
    void  addWhiteSpace(); // add nodes related to whiteSpace Detection
    void  addComment(); // add nodes related to Comment Detection
    Trie(const char* opFileName);
    ~Trie();
};


#endif
