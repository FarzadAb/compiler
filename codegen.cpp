#include "codegen.hpp"
#include <sstream>
#include <fstream>


CodeGenerator::CodeGenerator() {
    firstEmptyStatic = STATIC_FIELD_START;
    firstEmptyTemp = TEMP_VARS_START;
    returnVariable = getNextStatic();
    lastType = DT_INT;
}

void CodeGenerator::createIRCode() {
    pb.createIRCode();
}

void CodeGenerator::setErrorHandler(ErrorHandler* errorHandler) {
    this->errorHandler = errorHandler;
    pb.setErrorHandler(errorHandler);
}

constexpr unsigned int str2int(const char* str, int h = 0)
{
    return !str[h] ? 5381 : (str2int(str, h+1) * 33) ^ str[h];
}

void CodeGenerator::action(string semanticRoutine, string input){
    if( errorHandler->numErrors )
        return;
    // cerr << "Performing action : " << semanticRoutine << " on " << input << endl;
    switch( str2int(semanticRoutine.c_str()) ) {
        case str2int("mainVar") : {
            semanticStack.push_back(Address(pb.reserveCommand()));
            break;
        }

        case str2int("jmpMain") : {
            Address comm = semanticStack.back();
            semanticStack.pop_back();
            pb.setCommand(comm.value, Command(C_JP, Address(pb.nextCommandPosition())));
            break;
        }

        case str2int("createClass") : {
            classes[input] = Class();
            currentClass = input;
            break;
        }

        case str2int("createField") : {
            if( classes[currentClass].fields.count(input) )
                errorHandler->inform(ERR_SEM, "field " + input + " in class " + currentClass + " was declared once before");
            classes[currentClass].fields[input] = Variable(lastType, getNextStatic());
            break;
        }

        case str2int("createMethod") : {
            if( classes[currentClass].methods.count(input) )
                errorHandler->inform(ERR_SEM, "method " + input + " in class " + currentClass + " was declared once before");
            classes[currentClass].methods[input] = Method(lastType, pb.nextCommandPosition(), getNextStatic());
            currentMethod = input;
            break;
        }

        case str2int("createVar") : {
            if( classes[currentClass].methods[currentMethod].vars.count(input) )
                errorHandler->inform(ERR_SEM, "variable " + input + " has been declared before in method: " + currentClass + "." + currentMethod);
            else {
                classes[currentClass].methods[currentMethod].vars[input] = Variable(lastType, getNextStatic());
                if( classes[currentClass].methods[currentMethod].args.empty() || classes[currentClass].methods[currentMethod].args.back() != input )
                    pb.addCommand(Command(C_ASSIGN, Address(0, AT_IMMEDIATE), Address(firstEmptyStatic-VAR_SIZE)));
            }
            // cerr << "created var " << input << " at " << firstEmptyStatic-1 << endl;
            break;
        }

        case str2int("addArg") : {
            classes[currentClass].methods[currentMethod].args.push_back(input);
            break;
        }

        case str2int("setType") : {
            if( input == "int" )
                lastType = DT_INT;
            else
                lastType = DT_BOOLEAN;
            break;
        }

        case str2int("setSuper") : {
            if( classes.count(input) == 0 )
                errorHandler->inform(ERR_SEM, "super class " + input + " for class " + currentClass + " has not been created");
            else
                classes[currentClass].superClass = input;
            break;
        }

        case str2int("pushId") : {
            idStack.push(input);
            break;
        }

        case str2int("resolveId") : {
            // for(int i=semanticStack.size()-1; i>=0;i--)
            //     cerr << "\t" << semanticStack[i].value << endl;
            // cerr << "------------" << endl;
            Address addr;
            string identifier = idStack.top();
            idStack.pop();
            if( classes[currentClass].methods[currentMethod].vars.count(identifier) )
                addr = classes[currentClass].methods[currentMethod].vars[identifier].getAddress();
            else {
                string par = currentClass;
                while( par != "" ) {
                    if( classes[par].fields.count(identifier) ) {
                        addr = classes[par].fields[identifier].getAddress();
                        break;
                    }
                    par = classes[par].superClass;
                }
                if( addr.value == -1 ) {
                    errorHandler->inform(ERR_SEM, "unknown identifier " + identifier + " in method: " + currentClass + "." + currentMethod);
                    break;
                }
            }
            semanticStack.push_back(addr);
            // for(int i=semanticStack.size()-1; i>=0;i--)
            //     cerr << "\t" << semanticStack[i].value << endl;
            // cerr << "------------" << endl;
            break;
        }

        case str2int("resolveField") : {  // is not correct, need to create a stack
            string fieldName = idStack.top();
            idStack.pop();
            string cName = idStack.top();
            idStack.pop();

            if( classes.count(cName) == 0 )
                errorHandler->inform(ERR_SEM, "unknown class " + cName);
            else {
                string par = cName;
                while( par != "" ) {
                    if( classes[par].fields.count(fieldName) ) {
                        semanticStack.push_back(classes[par].fields[fieldName].getAddress());
                        break;
                    }
                    par = classes[par].superClass;
                }
                if( par == "" )
                    errorHandler->inform(ERR_SEM, "unknown field " + fieldName + " in class: " + cName);
            }
            break;
        }

        case str2int("resolveMethod") : {  // is not correct, need to create a stack
            // for(int i=semanticStack.size()-1; i>=0;i--)
            //     cerr << "\t" << semanticStack[i].value << endl;
            // cerr << "------------" << endl;
            string methodName = idStack.top();
            idStack.pop();
            string cName = idStack.top();
            idStack.pop();

            if( classes.count(cName) == 0 )
                errorHandler->inform(ERR_SEM, "unknown class " + cName);
            else {
                string par = cName;
                while( par != "" ) {
                    if( classes[par].methods.count(methodName) ) {
                        Method m = classes[par].methods[methodName];
                        // cerr << "num args: " << m.args.size() << endl;
                        for(int i=(int)m.args.size()-1; i>=0; i--)
                            if( m.vars[m.args[i]].type == semanticStack.back().dType ) {
                                pb.addCommand(Command(C_ASSIGN, semanticStack.back(), m.vars[m.args[i]].getAddress()));
                                semanticStack.pop_back();
                            }
                            else {
                                errorHandler->inform(ERR_SEM, "type of input argument number "
                                    + to_string(i) + " of method " + par + "." + methodName + "differ");
                            }
                        pb.addCommand(Command(C_ASSIGN, Address(pb.nextCommandPosition()+2, AT_IMMEDIATE),
                                                        Address(m.returnAddressVariable)));
                        pb.addCommand(Command(C_JP, m.getJumpAddress()));
                        Address tempReturn = Address(getNextTemp(), AT_DIRECT, m.type);
                        pb.addCommand(Command(C_ASSIGN, Address(returnVariable, AT_DIRECT), tempReturn));
                        semanticStack.push_back(tempReturn);
                        // for(int i=semanticStack.size()-1; i>=0;i--)
                        //     cerr << "\t" << semanticStack[i].value << endl;
                        // cerr << "------------" << endl;
                        break;
                    }
                    par = classes[par].superClass;
                }
                if( par == "" )
                    errorHandler->inform(ERR_SEM, "unknown method " + methodName + " in class: " + cName);
            }

            break;
        }

        case str2int("setRV") : {
            if( semanticStack.back().dType != classes[currentClass].methods[currentMethod].type )
                errorHandler->inform(ERR_SEM, "return value in method " + currentClass + "." + currentMethod + " does not match method return type");
            else {
                pb.addCommand(Command(C_ASSIGN, semanticStack.back(), Address(returnVariable, AT_DIRECT)));
                pb.addCommand(Command(C_JP, Address(classes[currentClass].methods[currentMethod].returnAddressVariable, AT_INDIRECT)));
                semanticStack.pop_back();
            }
            break;
        }

        case str2int("pushBoolLiteral") : {
            bool value = ( input == "true" );
            semanticStack.push_back(Address(value, AT_IMMEDIATE, DT_BOOLEAN));
            break;
        }

        case str2int("pushIntLiteral") : {
            int value = stoi(input);
            semanticStack.push_back(Address(value, AT_IMMEDIATE, DT_INT));
            break;
        }

        case str2int("equal") : {
            Address rhs = semanticStack.back();
            semanticStack.pop_back();
            if( rhs.dType != semanticStack.back().dType ) {
                errorHandler->inform(ERR_SEM, "in equal relation, type of variables differ");
                break;
            }
            Address t = Address(getNextTemp(), AT_DIRECT, DT_BOOLEAN);
            pb.addCommand(Command(C_EQ, semanticStack.back(), rhs, t));
            semanticStack.pop_back();
            semanticStack.push_back(t);
            break;
        }

        case str2int("lessThan") : {
            Address rhs = semanticStack.back();
            semanticStack.pop_back();
            if( rhs.dType != semanticStack.back().dType ) {
                errorHandler->inform(ERR_SEM, "in less than relation, type of variables differ");
                break;
            }
            Address t = Address(getNextTemp(), AT_DIRECT, DT_BOOLEAN);
            pb.addCommand(Command(C_LT, semanticStack.back(), rhs, t));
            semanticStack.pop_back();
            semanticStack.push_back(t);
            break;
        }

        case str2int("while") : {
            Address expr = semanticStack.back();
            semanticStack.pop_back();
            Address t = Address(getNextTemp(), AT_DIRECT, expr.dType);
            pb.addCommand(Command(C_NOT, expr, t));
            pb.addCommand(Command(C_JPF, t, semanticStack.back()));
            semanticStack.pop_back();
            break;
        }

        case str2int("jp") : {
            pb.setCommand(semanticStack.back().value, Command(C_JP, Address(pb.nextCommandPosition())));
            semanticStack.pop_back();
            break;
        }

        case str2int("jpfSave") : {
            int savedCommandP = semanticStack.back().value;
            semanticStack.pop_back();
            int newCommandP = pb.reserveCommand();
            pb.setCommand(savedCommandP, Command(C_JPF, semanticStack.back(), Address(newCommandP+1)));
            semanticStack.pop_back();
            break;
        }

        case str2int("save") : {
            semanticStack.push_back(Address(pb.reserveCommand()));
            break;
        }

        case str2int("and") : {
            Address rhs = semanticStack.back();
            semanticStack.pop_back();
            if( rhs.dType != semanticStack.back().dType ) {
                errorHandler->inform(ERR_SEM, "in and relation, type of variables differ");
                break;
            }
            Address t = Address(getNextTemp(), AT_DIRECT, rhs.dType);
            pb.addCommand(Command(C_AND, semanticStack.back(), rhs, t));
            semanticStack.pop_back();
            semanticStack.push_back(t);
            break;
        }

        case str2int("add") : {
            // for(int i=semanticStack.size()-1; i>=0;i--)
            //     cerr << "\t" << semanticStack[i].value << endl;
            // cerr << "------------" << endl;
            Address rhs = semanticStack.back();
            semanticStack.pop_back();
            if( rhs.dType != semanticStack.back().dType ) {
                errorHandler->inform(ERR_SEM, "in addition argument, type of variables differ");
                break;
            }
            Address t = Address(getNextTemp(), AT_DIRECT, rhs.dType);
            pb.addCommand(Command(C_ADD, semanticStack.back(), rhs, t));
            semanticStack.pop_back();
            semanticStack.push_back(t);
            // for(int i=semanticStack.size()-1; i>=0;i--)
            //     cerr << "\t" << semanticStack[i].value << endl;
            // cerr << "------------" << endl;
            break;
        }

        case str2int("subtract") : {
            Address rhs = semanticStack.back();
            semanticStack.pop_back();
            if( rhs.dType != semanticStack.back().dType ) {
                errorHandler->inform(ERR_SEM, "in subtract argument, type of variables differ");
                break;
            }
            Address t = Address(getNextTemp(), AT_DIRECT, rhs.dType);
            pb.addCommand(Command(C_SUB, semanticStack.back(), rhs, t));
            semanticStack.pop_back();
            semanticStack.push_back(t);
            break;
        }

        case str2int("assign") : {
            Address source = semanticStack.back();
            semanticStack.pop_back();
            if( source.dType != semanticStack.back().dType ) {
                // cerr << (source.dType == DT_BOOLEAN) << endl;
                // cerr << (semanticStack.back().dType == DT_BOOLEAN) << endl;
                // cerr << semanticStack.back().value << endl;
                errorHandler->inform(ERR_SEM, "in assignment argument, type of variables differ");
                break;
            }
            pb.addCommand(Command(C_ASSIGN, source, semanticStack.back()));
            semanticStack.pop_back();
            break;
        }

        case str2int("for") : {
            // for(int i=semanticStack.size()-1; i>=0;i--)
            //     cerr << "\t" << semanticStack[i].value << endl;
            // cerr << "------------" << endl;
            Address intValue = semanticStack.back();
            semanticStack.pop_back();
            Address id = semanticStack.back();
            semanticStack.pop_back();
            pb.addCommand(Command(C_ADD, id, intValue, id));
            int saveAddr = semanticStack.back().value;
            semanticStack.pop_back();
            Address expr = semanticStack.back();
            semanticStack.pop_back();
            pb.addCommand(Command(C_JP, semanticStack.back()));
            semanticStack.pop_back();
            pb.setCommand(saveAddr, Command(C_JPF, expr, Address(pb.nextCommandPosition())));
            // for(int i=semanticStack.size()-1; i>=0;i--)
            //     cerr << "\t" << semanticStack[i].value << endl;
            // cerr << "------------" << endl;
            break;
        }

        case str2int("label") : {
            semanticStack.push_back(Address(pb.nextCommandPosition()));
            break;
        }

        case str2int("print") : {
            pb.addCommand(Command(C_PRINT, semanticStack.back()));
            semanticStack.pop_back();
            break;
        }

        case str2int("getTemp") : {
            semanticStack.push_back(Address(getNextTemp()));
            break;
        }

        case str2int("case") : {
            int caseJPFAddr = semanticStack.back().value;
            semanticStack.pop_back();
            int caseEQAddr = semanticStack.back().value;
            semanticStack.pop_back();

            Address genExpr = semanticStack.back();
            semanticStack.pop_back();

            Address switchValue = semanticStack.back();
            semanticStack.pop_back();

            if( switchValue.dType != genExpr.dType ) {
                errorHandler->inform(ERR_SEM, "in case statement, key and case-value types do not match");
                break;
            }

            Address endJPaddr = semanticStack.back();
            Address temp = Address(getNextTemp(), AT_DIRECT);
            endJPaddr.type = AT_INDIRECT;

            pb.addCommand(Command(C_JP, endJPaddr));
            pb.setCommand(caseEQAddr, Command(C_EQ, genExpr, switchValue, temp));
            pb.setCommand(caseJPFAddr, Command(C_JPF, temp, Address(pb.nextCommandPosition())));

            semanticStack.push_back(switchValue);
            break;
        }

        case str2int("switch") : {
            semanticStack.pop_back();
            Address r = semanticStack.back();
            semanticStack.pop_back();
            int saveAddr = semanticStack.back().value;
            semanticStack.pop_back();

            pb.setCommand(saveAddr, Command(C_ASSIGN, Address(pb.nextCommandPosition(), AT_IMMEDIATE), r));
            break;
        }

        default :
            errorHandler->inform(WARN, "unknown semantic routine: " + semanticRoutine);
    }
}

ostream& operator <<(ostream& out, const CommandType& t) {
    switch(t) {
        case C_ADD: out << "ADD"; break;
        case C_AND: out << "AND"; break;
        case C_ASSIGN: out << "ASSIGN"; break;
        case C_EQ: out << "EQ"; break;
        case C_JPF: out << "JPF"; break;
        case C_JP: out << "JP"; break;
        case C_LT: out << "LT"; break;
        case C_MULT: out << "MULT"; break;
        case C_NOT: out << "NOT"; break;
        case C_PRINT: out << "PRINT"; break;
        case C_SUB: out << "SUB"; break;

        default : out << "*UNKNOWN*";
    }
    return out;
}

ostream& operator <<(ostream& out, const Address& addr) {
    if( addr.type == AT_IMMEDIATE )
        out << "#";
    else if ( addr.type == AT_INDIRECT )
        out << "@";
    return out << addr.value;
}

ostream& operator <<(ostream& out, const Command& c) {
    out << "(" << c.type << ", " << c.addr1;
    if( c.addr2.value != -1 ) {
        out << ", " << c.addr2;
        if( c.addr3.value != -1 )
            out << ", " << c.addr3;
    }
    return out << ")";
}

void ProgramBlock::createIRCode() {
    for(int i=0; i<(int)commands.size(); i++) {
        cerr << "PB[" << (i < 10 ? " " : "" ) << i <<  "]: ";
        cout << commands[i] << endl;
    }
}
