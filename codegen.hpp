#ifndef __CODEGEN__
#define __CODEGEN__ 1

#include <iostream>
#include <map>
#include <vector>
#include <stack>
#include <map>
#include "buffer.hpp"
#include "trie.hpp"
#include "token.hpp"
#include "scanner.hpp"
#include "error.hpp"
using namespace std;

enum DataType {
    DT_INT, DT_BOOLEAN
};

enum AddressType {
    AT_DIRECT, AT_INDIRECT, AT_IMMEDIATE
};

struct Address {
    int value;
    AddressType type;
    DataType dType;

    Address(int v=-1, AddressType t=AT_DIRECT, DataType dtype=DT_INT) : value(v), type(t), dType(dtype) {}
};

struct Variable {
    DataType type;
    int staticAddress;

    Variable() { staticAddress = -1; }
    Variable(DataType t, int addr) : type(t), staticAddress(addr) {}

    Address getAddress() { return Address(staticAddress, AT_DIRECT, type); }
};

enum CommandType {
    C_RESERVED, C_ADD, C_AND, C_ASSIGN, C_EQ, C_JPF, C_JP, C_LT, C_MULT, C_NOT, C_PRINT, C_SUB
};

struct Command {
    CommandType type;
    Address addr1, addr2, addr3;

    Command() : type(C_RESERVED) {}
    Command(CommandType t, Address a1, Address a2=Address(), Address a3=Address()) :
        type(t), addr1(a1), addr2(a2), addr3(a3) {}

};

struct ProgramBlock {
    // const static int PROGRAM_BLOCK_START = 0;
    vector<Command> commands;
    ErrorHandler* errorHandler;

    void addCommand(Command c) { commands.push_back(c); }

    int reserveCommand() {
        commands.push_back(Command());
        return commands.size()-1;
    }

    int nextCommandPosition() {
        return (int)commands.size();
    }

    void setErrorHandler(ErrorHandler* eh) { errorHandler = eh; }

    void setCommand(int p, Command c) {
        if( p < (int)commands.size() )
            commands[p] = c;
        else
            errorHandler->inform(WARN, "Illegal setCommand index requested");
    }

    void createIRCode();
};

struct Method {
    DataType type;
    int startAddress;
    vector<string> args;
    map<string, Variable> vars;
    int returnAddressVariable;

    Method() { startAddress = -1; }
    Method(DataType t, int addr, int retAddr) : type(t), startAddress(addr), returnAddressVariable(retAddr) {}

    Address getJumpAddress() {
        return Address(startAddress);
    }
};

struct Class {
    string superClass;
    map<string, Variable> fields;
    map<string, Method> methods;
    Class(string super="") : superClass(super) {}
};

struct CodeGenerator{
    const static int STATIC_FIELD_START = 100;
    const static int TEMP_VARS_START = 500;
    const static int VAR_SIZE = 4;
    ErrorHandler* errorHandler;
    vector<Address> semanticStack;
    stack<string> idStack;
    map<string, Class> classes;
    string currentClass;
    string currentMethod;
    DataType lastType;
    int returnVariable;
    int firstEmptyStatic;
    int firstEmptyTemp;
    ProgramBlock pb;

    CodeGenerator();
    int getNextStatic() { firstEmptyStatic += VAR_SIZE; return firstEmptyStatic-VAR_SIZE; }
    int getNextTemp() { firstEmptyTemp += VAR_SIZE; return firstEmptyTemp-VAR_SIZE; }
    void action(string semanticRutine, string input);
    void createIRCode();
    void setErrorHandler(ErrorHandler* errorHandler);
    // Address returnAddress() { return Address(returnVariable, AT_DIRECT); }
};

#endif
