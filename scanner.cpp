#include "scanner.hpp"
#include <sstream>

void Scanner::setErrorHandler(ErrorHandler* errorHandler){this->errorHandler=errorHandler;}

int strToInt(string s) {
    stringstream ss(s);
    int a;
    ss >> a;
    return a;
}

Scanner::Scanner(istream* in) {
    buffer = new Buffer(in);
    init();
}

Scanner::Scanner(const char* fileName) {
    buffer = new Buffer(fileName);
    init();
}

void Scanner::init() {
    trie = new Trie("operators.txt");
    ifstream fin("keywords.txt");
    string kw;
    while(fin >> kw){
        keywords[kw] = keywords.size();
    }
}

void Scanner::addIdentifier(string id) {
    if( identifiers.find(id) == identifiers.end() ) {
        identifiers[id] = identifiers.size();
    }
}

Scanner::~Scanner() {
    delete buffer;
    delete trie;
}

Token Scanner::getNextToken() {
    char c = buffer->peekChar();
    if( c == -1 )
        return Token(KIND_DOLLAR, "$");
    string lexeme = "";
    while(true) {
        // cerr << "current lexeme " << lexeme << endl;
        c = buffer->peekChar();
        // cerr << "peekChar: " << c << " " << (int)c << endl;
        TokenState ret = trie->nextStep(c);
        // cerr << "nextStep: " << ret.tState << " " << ret.tType << endl;

        if( ret.tState != STATE_OK_CHARBACK ) {
            buffer->skipChar();
            lexeme += c;
        }
        // else
        //     cerr << "putting char " << c << " back" << endl;
        // cerr << "current lexeme 2 " << lexeme << endl;

        if( ret.tState == STATE_ERROR ) {
            errorHandler->inform(ERR_SCAN,"error on input, when reading: '"+lexeme+"'");
            // TODO: add line and character
            lexeme = "";
            continue;
        }
        // cerr << "should get to here 3!" << endl;
        if( ret.tState == STATE_NOTHING_YET )
            continue;
        // cerr << "should get to here 2!" << endl;
        if( ret.tType == KIND_WHITESPACE ||
            ret.tType == KIND_ONELINECOMMENT ||
            ret.tType == KIND_MULTILINECOMMENT )
        {
            // cerr << "found a whitespace, nowPT==head is:" << (trie->nowPT == trie->head) << endl;
            lexeme = "";
            if( c == -1 )
                return Token(KIND_DOLLAR, "$");
            continue;
        }
        // cerr << "should get to here!" << endl;
        if( ret.tType == KIND_OP ) {
            if( lexeme[(int)lexeme.size()-1] != ')' )
                trie->lastTokenSPOperator = true;
            else
                trie->lastTokenSPOperator = false;
            return Token(ret.tType, lexeme);
        }
        trie->lastTokenSPOperator = false;
        if( ret.tType == KIND_INT )
            return Token(ret.tType, lexeme, strToInt(lexeme));

        if( ret.tType != KIND_ID ) {
            // cout << "something went wrong! skipping input" << endl;
            lexeme = "";
            trie->nowPT = trie->head;
            continue;
        }

        if( keywords.find(lexeme) != keywords.end() )
            return Token(KIND_KEYWORD, lexeme, keywords[lexeme]);
        addIdentifier(lexeme);
        return Token(KIND_ID, lexeme, identifiers[lexeme]);
    }
}
