#include "trie.hpp"

Trie::Trie(const char* opFileName){
    lastTokenSPOperator = false;
    head=new Node();
    addIdentifier();
    addInteger();
    nowPT=head;

    ifstream fin(opFileName);
    string op;
    while(fin >> op){
        addString(op, KIND_OP);
        for(int i=0;i<(int)op.size();i++)
            legalChars.insert(op[i]);
    }
    for(char c='a'; c<='z';c++)
        legalChars.insert(c);
    for(char c='A'; c<='Z';c++)
        legalChars.insert(c);
    for(char c='0'; c<='9';c++)
        legalChars.insert(c);

    legalChars.insert('/');
    legalChars.insert('*');

    whiteSpace.insert(' ');
    whiteSpace.insert('\t');
    whiteSpace.insert('\n');
    whiteSpace.insert('\r');
    whiteSpace.insert('\v');
    whiteSpace.insert('\f');

    addWhiteSpace();
    addComment();
}

Trie::~Trie(){
    head->recursiveDelete();
    delete head;
}

bool Node::recursiveDelete(){
    if( markedForDeath )
        return false;
    markedForDeath = true;
    for(map<char,Node*>::iterator it=child.begin(); it!=child.end(); it++){
        Node *pt=it->second;
        if( pt->recursiveDelete() )
            delete pt;
    }
    return true;
}

Node::Node(){
    markedForDeath = false;
    child.clear();
    kind = KIND_NOTHING_YET;
}

void Trie::addWhiteSpace() {
    Node* whiteSpaceNode = new Node();
    whiteSpaceNode->kind = KIND_WHITESPACE;

    for(set<char>::iterator it=whiteSpace.begin(); it!=whiteSpace.end(); it++){
        head->child[*it] = whiteSpaceNode;
        whiteSpaceNode->child[*it] = whiteSpaceNode;
    }
}

void Trie::addComment(){
    Node* commentStartNode = new Node();
    commentStartNode->kind = KIND_NOTHING_YET;
    head->child['/'] = commentStartNode;

    Node* oneLineCommentNode = new Node();
    oneLineCommentNode->kind = KIND_ONELINECOMMENT;
    commentStartNode->child['/'] = oneLineCommentNode;
    for(set<char>::iterator it=legalChars.begin(); it!=legalChars.end(); it++)
        oneLineCommentNode->child[*it] = oneLineCommentNode;
    for(set<char>::iterator it=whiteSpace.begin(); it!=whiteSpace.end(); it++)
        if( *it != '\n' )
            oneLineCommentNode->child[*it] = oneLineCommentNode;

    Node* multiLineCommentNode = new Node();
    multiLineCommentNode->kind = KIND_MULTILINECOMMENT;
    commentStartNode->child['*'] = multiLineCommentNode;

    Node* multiLineCommentEndNode = new Node();
    multiLineCommentEndNode->kind = KIND_MULTILINECOMMENT;

    for(set<char>::iterator it=legalChars.begin(); it!=legalChars.end(); it++){
        multiLineCommentNode->child[*it] = multiLineCommentNode;
        multiLineCommentEndNode->child[*it] = multiLineCommentNode;
    }
    for(set<char>::iterator it=whiteSpace.begin(); it!=whiteSpace.end(); it++){
        multiLineCommentNode->child[*it] = multiLineCommentNode;
        multiLineCommentEndNode->child[*it] = multiLineCommentNode;
    }

    multiLineCommentNode->child['*'] = multiLineCommentEndNode;
    multiLineCommentEndNode->child['*'] = multiLineCommentEndNode;
    Node* commentEndNode = new Node();
    commentEndNode->kind = KIND_ONELINECOMMENT;
    oneLineCommentNode->child['\n'] = commentEndNode;
    multiLineCommentEndNode->child['/'] = commentEndNode;
}

void Trie::addIdentifier(){
    Node* idNode=new Node();
    for(char c='a'; c<='z';c++){
        head->child[c]=idNode;
        head->child[c-'a'+'A']=idNode;
    }
    for(char c='a'; c<='z';c++)
        idNode->child[c]=idNode;
    for(char c='A'; c<='Z';c++)
        idNode->child[c]=idNode;
    for(char c='0'; c<='9';c++)
        idNode->child[c]=idNode;
    idNode->kind=KIND_ID;
}

void Trie::addInteger(){
    // Node* pt0=addString("+",0);
    // Node* pt1=addString("-",0);
    Node* node=new Node();
    for(char c='0'; c<='9';c++){
        // pt0->child[c]=node;
        // pt1->child[c]=node;
        head->child[c] = node;
        node->child[c] = node;
    }
    node->kind=KIND_INT;
}

Node* Trie::addString(string in,TokenType kind){
    Node* pt=head;
    for(int i=0;i<(int)in.size();i++){
        if(pt->child.find(in[i])!=pt->child.end())
            pt=pt->child[in[i]];
        else{
            Node* next=new Node();
            pt->child[in[i]]=next;
            pt=next;
        }
    }
    pt->kind = kind;
    return pt;
}

TokenState Trie::nextStep(char c){
    // cerr << "we are at head is: " << (nowPT == head) << " and nextStep char " << c << endl;
    // cerr << "nowPT->child[c] is: " << nowPT->child.count(c) << endl;
    // cerr << "char " << c << " and kind till now is: " << nowPT->kind << " nowPT" << nowPT << endl;
    if( c == -1 ) {
        return TokenState(nowPT->kind, STATE_OK_CHARBACK);
        // TODO: check if nowPT->kind is nothing yet
    }
    if(legalChars.find(c)==legalChars.end() && whiteSpace.find(c)==whiteSpace.end()){
        nowPT=head;
        return TokenState(KIND_NOTHING_YET, STATE_ERROR);
    }
    if(nowPT->child.find(c)!=nowPT->child.end()){
        nowPT=nowPT->child[c];
        if( nowPT->child.size()==0 &&
            (nowPT->kind == KIND_ONELINECOMMENT))
        {
            nowPT=head;
            return TokenState(nowPT->kind, STATE_OK);
        }
        return TokenState(KIND_NOTHING_YET, STATE_NOTHING_YET);
    }else{
        if( nowPT->kind == KIND_OP ) {
            if( (c >= '0' && c <= '9') && lastTokenSPOperator ) {
                nowPT = head->child[c];
                return TokenState(KIND_NOTHING_YET, STATE_NOTHING_YET);
            }
            else {
                TokenType out = nowPT->kind;
                nowPT=head;
                return TokenState(out, STATE_OK_CHARBACK);
            }
        }
        if( nowPT->kind == KIND_NOTHING_YET ||
            nowPT->kind == KIND_MULTILINECOMMENT /*not ending multiline*/)
        {
            nowPT=head;
            return TokenState(KIND_NOTHING_YET, STATE_ERROR);
        }
        if(nowPT->kind == KIND_INT){
            nowPT=head;
            if((c>='a' && c<='z') || (c>='A' && c<='Z')) {
                return TokenState(KIND_NOTHING_YET, STATE_ERROR);
            }
            return TokenState(KIND_INT,STATE_OK_CHARBACK);

        }
        if( nowPT->kind == KIND_ID ||
            nowPT->kind == KIND_WHITESPACE)
        {
            TokenType out = nowPT->kind;
            nowPT=head;
            return TokenState(out, STATE_OK_CHARBACK);
        }

        // we should never reach this line
        return TokenState(KIND_NOTHING_YET, STATE_NOTHING_YET);
    }
}
