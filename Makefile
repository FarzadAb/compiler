all: tester
	g++ -std=c++11 -Wall mainCompiler.cpp error.cpp parser.cpp scanner.cpp buffer.cpp trie.cpp token.cpp codegen.cpp semantic.cpp -o compiler

scanner:
	g++ main.cpp scanner.cpp buffer.cpp trie.cpp token.cpp -o scanner

tester:
	g++ tester.cpp -o tester

run: all
	./compiler -GgrammarFull.txt testCode.java

runp: all
	./compiler -pp -GgrammarFull.txt testCode.java

test: all
	./compiler -GgrammarFull.txt testCode.java >out.txt 2>/dev/null && ./tester <out.txt

zip:
	rm -f p2_92100375_92100404.zip
	zip p2_92100375_92100404.zip *.cpp *.hpp Makefile keywords.txt operators.txt test.txt testCode.java grammarFull.txt grammarRaw.txt grammarLL1.txt

clean:
	rm -f p2_92100375_92100404.zip scanner compiler
