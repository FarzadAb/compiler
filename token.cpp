#include "token.hpp"
#include "colors.hpp"

Token::Token(TokenType type, string lex, int det) {
    tokenType = type;
    lexeme = lex;
    details = det;
}
Token::Token(){}

ostream& operator <<(ostream& out, const Token& t) {
    if( Colors::prettyPrint ) {
        switch (t.tokenType) {
            case KIND_DOLLAR:
                return out << C_Red << "$" << C_Reset;
            case KIND_OP:
                return out << C_Blue << t.lexeme << C_Reset;
            case KIND_KEYWORD:
                return out << C_Green << t.lexeme << C_Reset;
            case KIND_INT:
                return out << C_Yellow << t.lexeme << C_Reset;
            default:
                return out << t.lexeme;
        }
    }
    else {
        switch (t.tokenType) {
            case KIND_DOLLAR:
                return out;
            case KIND_ID:
                return out << "identifier";
            case KIND_INT:
                return out << "integer";
            default:
                return out << t.lexeme;
        }
    }
}
