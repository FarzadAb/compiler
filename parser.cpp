#include "parser.hpp"
#include "colors.hpp"
#include <sstream>
#include <fstream>
//TODO epsilon in input
//TODO startID should be the first (ID=0)
bool UNION(TSET &S1,TSET S2);
bool INTERSECT(const TSET &S1,const TSET &S2){
    for(TSET::iterator it=S1.begin();it!=S1.end();it++)
        if(S2.count(*it)){
            cerr<<" we have intersection on token  "<<(*it).lexeme<<endl;
            return true;
        }
    return false;
}

void Parser::setErrorHandler(ErrorHandler *errorHandler){
    this->errorHandler=errorHandler;
    IRgen->setErrorHandler(errorHandler);
}

void Parser::createIRCode() {
    IRgen->createIRCode();
}

bool Parser::checkLL1(){
    bool LL1=true;
    for(int k=0;k<(int)nonTerminals.size();k++){
        NonTerminal NT=nonTerminals[k];
        vector<TSET> V;
        for(int i=0;i<(int)NT.rules.size();i++){
            LFS RHS=rules[NT.rules[i]].RHS;
            TSET now=findFirst(RHS);
            if(now.count(epsilon))
                UNION(now,Follow[NT]);
            V.PB(now);
        }
        for(int i=0;i<(int)V.size();i++)
            for(int j=i+1;j<(int)V.size();j++)
                if(INTERSECT(V[i],V[j])){
                    cerr<<"Grammer is not LL(1) because of rules number "
                        <<NT.rules[i]+1<<" and "<<NT.rules[j]+1<<" for nonTerminal "
                        <<setRed<<NT.lexeme<<doReset<<endl;
                    LL1=false;
                }
        }
        return LL1;
}

Parser::Parser(const char* grammer){
    int epsID=registerT("");
    int dollarID=registerT("$");
    dollar=terminals[dollarID];
    epsilon=terminals[epsID];
    IRgen = new CodeGenerator();
    startID=0;
    init(grammer);
    findFirst();
    findFollow();

    //Dbug info
    printNonTerminals();
    printTerminals();
    printSemanticRutines();
    printRules();
    printFirst();
    printFollow();

}

void Parser::setScanner(Scanner* scanner){this->scanner=scanner;}

void Parser::compile(){
    LA=scanner->getNextToken();
    parse(nonTerminals[startID]);
    if(LA.tokenType!=KIND_DOLLAR)
        errorHandler->inform(ERR_PARS, "we Expected input to end (EOF) but we found "+LA.lexeme);
}

string getCurrentToken(Token T){
    string currentToken;
    if(T.tokenType==KIND_ID)
        currentToken="identifier";
    else if(T.tokenType==KIND_INT)
        currentToken="integer";
    else
        currentToken=T.lexeme;
    return currentToken;
}

void Parser::parse(NonTerminal NT){
    while(true){
            for(int i=0;i<(int)NT.rules.size();i++){
                LFS RHS=rules[NT.rules[i]].RHS;
                TSET now=findFirst(RHS);
                if(now.count(epsilon))
                    UNION(now,Follow[NT]);
                if(now.count(*(new Terminal(getCurrentToken(LA))))){             // Match Found
                    for(int j=0;j<(int)RHS.elements.size();j++)
                        if(RHS.elements[j].SS==EL_NT)                // NonTerminal
                            parse(nonTerminals[RHS.elements[j].FF]);
                        else if(RHS.elements[j].SS==EL_T)            // Terminal
                            match(terminals[RHS.elements[j].FF]);
                        else                                         // Semantic Rutine
                            IRgen->action(rutines[RHS.elements[j].FF].lexeme, LA.lexeme);
                    return;
                }
            }
            if(Follow[NT].count(*(new Terminal(LA.lexeme))) || LA.tokenType==KIND_DOLLAR){ //PANIC MODE
                errorHandler->inform(ERR_PARS,"we expected something produced by "+NT.lexeme+" here but it was missing so we assumed there is something produced by "+NT.lexeme+" here");
                return;
            }
            errorHandler->inform(ERR_PARS,"we didn't expect "+LA.lexeme+" here so we removed it from input");
            LA=scanner->getNextToken();
    }
}
void Parser::match(Terminal t){
        if(t.lexeme=="" || t==epsilon)
            return;
        if(getCurrentToken(LA)==t.lexeme)
                LA=scanner->getNextToken();
        else{ //PANIC MODE
            errorHandler->inform(ERR_PARS,"Syntax Error : we Expected "+t.lexeme+" but we found "+LA.lexeme+" so we added "+t.lexeme+" to the input");
        }
}

int Parser::registerSR(string lexeme){
    SemanticRutine newSR(lexeme);
    if(SR2i.count(newSR)){
        return (SR2i.find(newSR)->FF).id;
    }else{
        newSR.id=SR2i.size();
        SR2i[newSR]=newSR.id;
        rutines.PB(newSR);
        lexeme2type[lexeme]=EL_SR;
        return newSR.id;
    }
    return 0;
}

int Parser::registerNT(string lexeme){
    NonTerminal newNT(lexeme);
    if(NT2i.count(newNT)){
        return (NT2i.find(newNT)->FF).id;
    }else{
        newNT.id=NT2i.size();
        NT2i[newNT]=newNT.id;
        nonTerminals.PB(newNT);
        lexeme2type[lexeme]=EL_NT;
        return newNT.id;
    }
    return 0;
}

int Parser::registerT(string lexeme){
    Terminal newT(lexeme);
    if(lexeme=="epsilon")
        return T2i[epsilon];
    if(T2i.count(lexeme)){
        return (T2i.find(newT)->FF).id;
    }else{
        newT.id=T2i.size();
        T2i[newT]=newT.id;
        terminals.PB(newT);
        lexeme2type[lexeme]=EL_T;
        return newT.id;
    }
    return 0;
}

void Parser::init(const char* grammer){
    ifstream fin(grammer);
    while(true){
        string now;
        if( !(fin>>now) )
            break;
        Rule newRule;
        if(now[0]!='$'){
            cerr<<"Error in grammer specification file!\n LHS should start with $"<<endl;
            return ;
        }
        int LHSid=registerNT(now.substr(1));
        newRule.LHS=nonTerminals[LHSid];
        nonTerminals[LHSid].rules.PB((int)rules.size());

        fin>>now;
        if(now!="->"){
            cerr<<"Error in grammer specification file!\n -> symbol needed"<<endl;
        }
        LFS newLFS;
        while(true){
            fin >> now;
            PIET temp;
            if(now[0]=='|'){
                newRule.RHS=newLFS;
                rules.PB(newRule);
                break;
            }
            if(now[0]=='$'){            // NonTerminal has been found
                temp.FF=registerNT(now.substr(1));
                temp.SS=EL_NT;
                newLFS.elements.PB(temp);
            }else if(now[0]=='#'){      // SemanticRutine has been found
                temp.FF=registerSR(now.substr(1));
                temp.SS=EL_SR;
                newLFS.elements.PB(temp);
            }
            else{                       // Terminal has been found
                temp.FF=registerT(now.substr(0));
                temp.SS=EL_T;
                newLFS.elements.PB(temp);
            }
        }

    }
}

void Parser::findFirst(){
    while(true){
        isEndFirst=true;
        mrk.clear();
        for(int i=0;i<(int)nonTerminals.size();i++)
            if(!mrk[nonTerminals[i]])
                findFirst(PIET(nonTerminals[i].id,EL_NT));
        if(isEndFirst)
            break;
    }
}

bool UNION(TSET &S1,TSET S2){
    bool ret=false;
    for(TSET::iterator it=S2.begin();it!=S2.end();it++){
        if(!S1.count(*it))
            ret=true;
        S1.insert(*it);
    }
    return ret;
}

TSET Parser::findFirst(PIET element){
    if(element.SS==EL_T){

        TSET ret;
        ret.insert(terminals[element.FF]);
        return ret;
    }else{
        TSET ret;
        NonTerminal NT=nonTerminals[element.FF];
        if(mrk[NT])
            return First[NT];
        mrk[NT]=true;
        for(int i=0;i<(int)NT.rules.size();i++)
            UNION(ret, findFirst(rules[NT.rules[i]].RHS));
        if(UNION(First[NT],ret))
            isEndFirst=false;
        return First[NT];
    }
}

LFS removeSR(LFS phrase){
    LFS ret;
    for(int i=0;i<(int)phrase.elements.size();i++)
        if(phrase.elements[i].SS!=EL_SR)
            ret.elements.PB(phrase.elements[i]);
    return ret;
}

TSET Parser::findFirst(LFS phrase){
    phrase = removeSR(phrase);
    TSET ret;
    if(phrase.elements.size()==0)
        ret.insert(epsilon);
    for(int i=0;i<(int)phrase.elements.size();i++){
        TSET temp=findFirst(phrase.elements[i]);
        if(temp.count(epsilon)){
            if(i==(int)phrase.elements.size()-1){
                UNION(ret,temp);
                break;
            }else{
                temp.erase(epsilon);
                UNION(ret,temp);
            }
        }else{
            UNION(ret,temp);
            break;
        }
    }
    return ret;
}

void Parser::findFollow(){
    Follow[nonTerminals[startID]].insert(dollar);
    while(true){
        isEndFollow=true;
        mrk.clear();
        for(int i=0;i<(int)rules.size();i++){
            NonTerminal LHS=rules[i].LHS;
            LFS RHS=rules[i].RHS;
            RHS=removeSR(RHS);
            for(int j=0;j<(int)RHS.elements.size();j++){
                if(RHS.elements[j].SS==EL_NT){
                    NonTerminal NT=nonTerminals[RHS.elements[j].FF];
                    LFS Rest=RHS.trim(j+1);
                    if(findFirst(Rest).count(epsilon))
                        if(UNION(Follow[NT],Follow[LHS]))
                            isEndFollow=false;
                    TSET temp=findFirst(Rest);
                    temp.erase(epsilon);
                    if(UNION(Follow[NT], temp))
                        isEndFollow=false;
                }
            }
        }
        if(isEndFollow)
            break;
    }
}

void Parser::printFollow(){
    cerr<<"\n"<<setBlue<<"Follows"<<doReset<<" :"<<endl;
    for(int i=0;i<(int)nonTerminals.size();i++){
        NonTerminal &NT= nonTerminals[i];
        cerr<<"Follow["<<NT.lexeme<<"] = { ";
        for(TSET::iterator it=Follow[NT].begin();it!=Follow[NT].end();it++)
            if((*it).lexeme!="$")
                cerr<<(*it).lexeme<<" ";
            else
                cerr<<"$ ";
        cerr<<"}"<<endl;
    }
}

void Parser::printFirst(){
    cerr<<"\n"<<setBlue<<"Firsts"<<doReset<<" :"<<endl;
    for(int i=0;i<(int)nonTerminals.size();i++){
        NonTerminal &NT= nonTerminals[i];
        cerr<<"First["<<NT.lexeme<<"] = { ";
        for(TSET::iterator it=First[NT].begin();it!=First[NT].end();it++)
            if((*it).lexeme!="")
                cerr<<(*it).lexeme<<" ";
            else
                cerr<<"[epsilon] ";
        cerr<<"}"<<endl;
    }
}

void Parser::printTerminals(){
    cerr<<"\n"<<setBlue<<"Terminals"<<doReset<<" : "<<endl;
    for(int i=0;i<(int)terminals.size();i++)
        cerr<<"(id:"<<terminals[i].id<<",lexeme:"<<terminals[i].lexeme<<") ";
    cerr<<endl;
}
void Parser::printNonTerminals(){
    cerr<<setBlue<<"NonTerminals"<<doReset<<" : "<<endl;
    for(int i=0;i<(int)nonTerminals.size();i++)
        cerr<<"(id:"<<nonTerminals[i].id<<",lexeme:"<<nonTerminals[i].lexeme<<") ";
    cerr<<endl;
}
void Parser::printSemanticRutines(){
    cerr<<"\n"<<setBlue<<"SemanticRuntines"<<doReset<<" : "<<endl;
    for(int i=0;i<(int)rutines.size();i++)
        cerr<<"(id:"<<rutines[i].id<<",lexeme:"<<rutines[i].lexeme<<") ";
    cerr<<endl;
}
void Parser::printRules(){
    cerr<<"\n"<<setBlue<<"Rules"<<doReset<<" : "<<endl;
    for(int i=0;i<(int)rules.size();i++)
        rules[i].print();
}
