#ifndef __SCANNER_TOKEN__
#define __SCANNER_TOKEN__ 1

#include <iostream>
using namespace std;

enum TokenType {
    KIND_NOTHING_YET,
    KIND_INT,
    KIND_ID,
    KIND_OP,
    KIND_ONELINECOMMENT,
    KIND_MULTILINECOMMENT,
    KIND_WHITESPACE,
    KIND_KEYWORD,
    KIND_DOLLAR
};

struct Token {
    static bool prettyPrint;
    TokenType tokenType;
    string lexeme;
    int details;
    Token(TokenType type, string lex, int det=-1);
    Token();
};

ostream& operator <<(ostream& out, const Token& t);

#endif
