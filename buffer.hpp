#ifndef __SCANNER_BUFFER__
#define __SCANNER_BUFFER__ 1

#include <iostream>
#include <fstream>
using namespace std;

struct Buffer {
    const static int Size = 1<<10;
    char buffer[2*Size+2];
    istream* file;
    int pointer;
    bool nextBuffRead;


    void putCharBack();

    char peekChar();
    void skipChar();

    void equip();

    void init();
    bool closeStream;  // says do we need to close the stream?
    Buffer(const char* fileName);
    Buffer(istream* in);
    ~Buffer();

    //
    string curLine;
    int line,col;
};

void testBuffer();

#endif
