#ifndef __PARSER__
#define __PARSER__ 1

#include <iostream>
#include <map>
#include <vector>
#include "buffer.hpp"
#include "trie.hpp"
#include "token.hpp"
#include "scanner.hpp"
#include "codegen.hpp"
#include "semantic.hpp"
#include "error.hpp"
using namespace std;

#define FF first
#define SS second
#define PB push_back

enum ElementType{
    EL_T,
    EL_NT,
    EL_SR
};
typedef pair<int,ElementType> PIET;


struct SemanticRutine{
    int id;
    string lexeme;
    bool operator <(const SemanticRutine& T)const{return lexeme < T.lexeme;}
    bool operator ==(const SemanticRutine& T)const{return lexeme == T.lexeme;}
    bool operator >(const SemanticRutine& T)const{return lexeme > T.lexeme;}
    SemanticRutine(){}
    SemanticRutine(string lexeme){this->lexeme=lexeme;}
};

struct Terminal{
    int id;
    string lexeme;
    bool operator <(const Terminal& T)const{return lexeme < T.lexeme;}
    bool operator ==(const Terminal& T)const{return lexeme == T.lexeme;}
    bool operator >(const Terminal& T)const{return lexeme > T.lexeme;}
    Terminal(){}
    Terminal(string lexeme){this->lexeme=lexeme;}
};
typedef set<Terminal> TSET;

struct NonTerminal{
    int id;
    string lexeme;
    vector<int> rules;
    bool operator <(const NonTerminal& T)const{return lexeme < T.lexeme;}
    bool operator ==(const NonTerminal& T)const{return lexeme == T.lexeme;}
    bool operator >(const NonTerminal& T)const{return lexeme > T.lexeme;}
    NonTerminal(){}
    NonTerminal(string lexeme){this->lexeme=lexeme;}
};

struct LFS{
    vector<PIET> elements;
    LFS trim(int s,int e=-1){
        if(e==-1)
            e=elements.size();
        LFS newLFS;
        for(int i=s;i<e;i++)
            newLFS.elements.PB(elements[i]);
        return newLFS;
    }
    void print(){
        for(int i=0;i<(int)elements.size();i++){
            string type;
            switch (elements[i].SS) {
                case EL_NT: type="NT";
                            break;
                case EL_T : type="T";
                            break;
                case EL_SR: type="SR";
                            break;
            }
            cerr<<"(type:"<<type<<",id:"<<elements[i].FF<<") ";
        }
        cerr<<endl;
    }
};

struct Rule{
    NonTerminal LHS;
    LFS         RHS;
    void print(){
        cerr<<"(type:NT,id:"<<LHS.id<<",lexeme:"<<LHS.lexeme<<") -> ";
        RHS.print();
    }
};

struct Parser {
    int startID;
    Scanner* scanner;
    SemanticAnalyser* semanticAnalayser;
    CodeGenerator* IRgen;
    ErrorHandler* errorHandler;

    void setErrorHandler(ErrorHandler* errorHandler);
    void setScanner(Scanner* scanner);
    void createIRCode();
    Token LA;
    void compile();
    void parse(NonTerminal NT);
    void match(Terminal T);

    //Grammer needs
    vector<Rule> rules;
    Terminal epsilon,dollar;

    map<SemanticRutine,int> SR2i;
    vector<SemanticRutine> rutines;

    map<Terminal,int> T2i;
    vector<Terminal> terminals;

    map<NonTerminal,int> NT2i;
    vector<NonTerminal>  nonTerminals;

    map<string,ElementType> lexeme2type;

    //First and Follow
    map<NonTerminal,TSET> First;
    map<NonTerminal,TSET> Follow;


    Parser(const char* grammer);
    void init(const char* grammer);
    int registerNT(string lexeme);
    int registerT (string lexeme);
    int registerSR(string lexeme);

    //First and Follow Computation
    bool isEndFirst;
    map<NonTerminal,bool> mrk;
    TSET findFirst(PIET element);
    TSET findFirst(LFS phrase);
    void findFirst();

    bool isEndFollow;
    void findFollow();

    bool checkLL1();

    //Debug tools
    void printFollow();
    void printFirst();
    void printRules();
    void printTerminals();
    void printNonTerminals();
    void printSemanticRutines();
};

#endif
