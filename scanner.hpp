#ifndef __SCANNER__
#define __SCANNER__ 1

#include <iostream>
#include <map>
#include "buffer.hpp"
#include "trie.hpp"
#include "token.hpp"
using namespace std;
struct ErrorHandler;

struct Scanner {
    Buffer* buffer;
    Trie* trie;
    map<string, int> keywords, identifiers;

    Scanner(const char*);
    Scanner(istream*);
    void init();
    Token getNextToken();
    void addIdentifier(string id);
    ~Scanner();

    //
    ErrorHandler* errorHandler;
    void setErrorHandler(ErrorHandler* errorHandler);
};
#include "error.hpp"
#endif
