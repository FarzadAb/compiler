#include <iostream>
#include "scanner.hpp"
#include "token.hpp"
#include "parser.hpp"
#include "semantic.hpp"
#include "codegen.hpp"
#include "error.hpp"
#include "colors.hpp"
using namespace std;

bool Colors::prettyPrint = false;

int main(int argc, char* argv[])
{
    ios::sync_with_stdio(false);

    Scanner* scanner = NULL;
    Parser* myParser = NULL; // dbug info can be canceled in the constructor
    for(int i=1; i<argc; i++) {
        string s(argv[i]);
        if( s == "-pp" )
            Colors::prettyPrint = true;
        else if( s.substr(0,2) == "-G" )
            myParser = new Parser(argv[i] + 2);
        else if( scanner == NULL )
            scanner = new Scanner(argv[i]);
    }
    if( scanner == NULL )
        scanner = new Scanner(&cin);
    if( myParser == NULL )
        myParser = new Parser("grammar2.txt");

    if( !myParser->checkLL1() ) {
        cerr << "\n" << setRed << "the grammar provided is not predictive (LL1)" << doReset << endl;
        return 0;
    }
    cerr << "\n" << setGreen << "__________it's LL(1)__________" << doReset << endl;

    ErrorHandler* errorHandler=new ErrorHandler(scanner);
    scanner->setErrorHandler(errorHandler);

    myParser->setScanner(scanner);   // set the scanner neeeded by parser
    myParser->setErrorHandler(errorHandler); // set the ErrorHandler
    cerr<<"\nStart Compiling :\n"<<endl;
    myParser->compile();

    if( errorHandler->numErrors == 0 ) {
        cerr << setGreen << "____________Accept____________" << doReset << endl;
        myParser->createIRCode();
    }
    else
        cerr << "we had " << setRed << errorHandler->numErrors << " Error(s)" << doReset << "." << endl;
    return 0;
}
