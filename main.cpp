#include <iostream>
#include "scanner.hpp"
#include "token.hpp"
using namespace std;

bool Token::prettyPrint = false;

int main(int argc, char* argv[])
{
    ios::sync_with_stdio(false);

    Scanner* scanner = NULL;
    for(int i=1; i<argc; i++) {
        string s(argv[i]);
        if( s == "-pp" )
            Token::prettyPrint = true;
        else if( scanner == NULL )
            scanner = new Scanner(argv[i]);
    }
    if( scanner == NULL )
        scanner = new Scanner(&cin);

    bool firstToken = true;
    cout << '[';
    while(true) {
        Token t = scanner->getNextToken();
        if( t.tokenType == KIND_DOLLAR ) {
            // cout << t << endl;
            break;
        }else if( firstToken )
            cout << t;
        else
            cout << ',' << t;
        firstToken = false;
    }
    cout << ']' << endl;
    return 0;
}
