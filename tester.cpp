#include <iostream>
#include <unistd.h>
#include <cstdlib>
#include <cstdio>
#include <algorithm>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <sstream>
using namespace std;
#define PB push_back
const int MAX_N=200;
vector<string> V;
map<int,int> M;
int PC=0;


int s2i(string str){
	int ret=0;
	for(int i=0;i<str.size();i++)
			ret=ret*10+(int)(str[i]-'0');
	return ret;
}

string i2s(int x){
	string ret;
	while(x){
		ret+=char(x%10);
		x/=10;
	}
	reverse(ret.begin(),ret.end());
	return ret;
}

int resolve(string param){
	if(param[0]=='#'){
		return s2i(param.substr(1));	
	}else if(param[0]=='@'){
		return M[M[s2i(param.substr(1))]];
	}else{
		return M[s2i(param)];	
	}
}

int resolveDest(string param){
	if(param[0]=='#'){
		return resolveDest(param.substr(1));
	}else if(param[0]=='@'){
		return M[s2i(param.substr(1))];
	}else{
		return s2i(param);
	}
}

void run(string command){
	cerr<<"--->  PC = "<<PC<<"\tcommand : "<<command<<endl;
	for(int i=0;i<command.size();i++)
		if(command[i]==',' || command[i]=='(' || command[i]==')')
				command[i]=' ';
	PC=PC+1;
	istringstream fin(command);
	string cmd;
	string t1,t2,t3;
	while(fin >> cmd){
		if(cmd=="ADD"){
			fin >> t1 >> t2 >> t3;
			M[resolveDest(t3)]=resolve(t1)+resolve(t2);
		}else if(cmd=="AND"){
			fin >> t1 >> t2 >> t3;
			M[resolveDest(t3)]=resolve(t1)+resolve(t2);
		}else if(cmd=="ASSIGN"){
			fin >> t1 >> t2;
			M[resolveDest(t2)]=resolve(t1);
		}else if(cmd=="EQ"){
			fin >> t1 >> t2 >> t3;
			M[resolveDest(t3)]=resolve(t1)==resolve(t2);
		}else if(cmd=="JPF"){
			fin >> t1 >> t2;
			if(resolve(t1)==0)
				PC=resolveDest(t2);
		}else if(cmd=="JP"){
			fin >> t1;
			PC=resolveDest(t1);
		}else if(cmd=="LT"){
			fin >> t1 >> t2 >> t3;
			M[resolveDest(t3)]=resolve(t1)<resolve(t2);
		}else if(cmd=="MULT"){
			fin >> t1 >> t2 >> t3;
			M[resolveDest(t3)]=resolve(t1)*resolve(t2);
		}else if(cmd=="NOT"){
			fin >> t1 >> t2;
			M[resolveDest(t2)]= !(resolve(t1));
		}else if(cmd=="PRINT"){
			fin >> t1;
			cout<<resolve(t1)<<endl;
		}else if(cmd=="SUB"){
			fin >> t1 >> t2 >> t3;
			M[resolveDest(t3)]=resolve(t1)-resolve(t2);
		}else{
		}
	}
}

int main()
{
	string temp;
	while(getline(cin,temp)){V.PB(temp);}
	while(PC<(int)V.size()){
		run(V[PC]);
		//sleep(3);
	}
	return 0;
}

