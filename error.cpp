#include "error.hpp"
#include "colors.hpp"

ErrorHandler::ErrorHandler(Scanner* scanner){
    numErrors = 0;
    this->scanner=scanner;
}
ErrorHandler::ErrorHandler(){
    numErrors = 0;
}

void ErrorHandler::inform(ErrorType type,string message){
    types.PB(type);
    messages.PB(message);

    numErrors++;

    int line=scanner->buffer->line;
    int col =scanner->buffer->col;
    string curLine=scanner->buffer->curLine;

    cerr<<endl;
    cerr<<"---> line:char=" << setRed << line+1<<":"<<col+1 << doReset <<" [input line]="<<curLine<<endl;

    switch (type) {
        case ERR_PARS:
            cerr<<"---> Syntax Error : "<<message<<endl;
            break;
        case ERR_SCAN:
            cerr<<"---> Lexical Error : "<<message<<endl;
            break;
        case ERR_SEM:
            cerr<<"---> Semantic Error : "<<message<<endl;
            break;
        case WARN:
            cerr<<"---> Warning : "<<message<<endl;
            break;
    }
    cerr<<endl;
}
