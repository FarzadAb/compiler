#include "buffer.hpp"

// void Buffer::putCharBack() {
//     pointer--;
//     if( pointer == -1 ) {
//         pointer = 2*Size;
//         nextBuffRead = true;
//     }
//     if( pointer == Size ) {
//         pointer--;
//         nextBuffRead = true;
//     }
// }

void Buffer::skipChar() {
    //
        if(buffer[pointer]=='\n'){
            line++;
            col=0;
            curLine="";
        }else{
            col++;
            curLine+=buffer[pointer];
        }
    //
    pointer++;
    if( buffer[pointer] < 0 ) {
        if( pointer == Size || pointer == 2*Size+1 )
            equip();
    }
}
char Buffer::peekChar() {
    return buffer[pointer];
}

void Buffer::equip() {
    pointer++;
    if( pointer == 2*Size+2 )
        pointer = 0;
    if( nextBuffRead ) {
        nextBuffRead = false;
        return;
    }
    file->read(buffer+pointer, Size);
    // cerr << "reading from file " << file->gcount() << " bytes" << endl;
    // cerr << "from " << pointer << " to " << pointer + file->gcount() << endl;
    buffer[pointer+file->gcount()] = -1;
}

Buffer::Buffer(const char* fileName) {
    file = new ifstream(fileName);
    closeStream = true;
    init();
}

Buffer::Buffer(istream* in) {
    file = in;
    closeStream = false;
    init();
}

void Buffer::init() {
    buffer[Size] = buffer[2*Size+1] = -1;
    pointer = -1;
    nextBuffRead = false;
    equip();
}

Buffer::~Buffer() {
    if( closeStream )
        ((ifstream*)file)->close();
}


void testBuffer() {
    // ios::sync_with_stdio(false);
    Buffer buff("salam.txt");
    while(true) {
        // char d = buff.nextChar();
        // if( d == -1 ) {
        //     cout << "this is the end 1!" << endl;
        //     break;
        // }
        // buff.putCharBack();
        char c = buff.peekChar();
        if( c == -1 ) {
            cerr << "this is the end!" << endl;
            break;
        }
        cout << "<" << c << ">" << endl;
        buff.skipChar();
    }
}
