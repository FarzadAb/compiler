# Compiler
This project contains a compiler for a restricted version of the Java language, written in C++.
If successful, it will create an intermediate representation of the code (like bytecode).
The grammar can also be changed by simply changing the `grammar.txt` file.

The compiler consists of four parts:
- Scanner: reads and tokenizes the input ([`scanner.cpp`](./scanner.cpp) and [`buffer.cpp`](./buffer.cpp));
- Parser: a [recursive descent](https://en.wikipedia.org/wiki/Recursive_descent_parser) style parser ([`parser.cpp`](./parsers.cpp));
  - It can check whether the input grammar is [LL(1)](https://en.wikipedia.org/wiki/Recursive_descent_parser) or not;
  - "Panic mode" error generation ([`error.cpp`](./error.cpp));
- Semantic analyzer ([`semantic.cpp`](./semantic.cpp));
- Intermediate code generator ([`codegen.cpp`](./codegen.cpp)).

## Usage
The `Makefile` has some handy commands that can be useful:
- `make all`: compiles the code itself;
- `make runp`: runs the code on a pre-specified test case (`testCode.java`);
- `make test`: runs the output of the intermediate code that was generated in the previous step for testing purposes.