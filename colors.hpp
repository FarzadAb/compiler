#ifndef __COLORS__
#define __COLORS__ 1

#include "iostream"
using namespace std;

const string C_Reset  = "\033[00m";
const string C_Green  = "\033[01;32m";
const string C_Red    = "\033[01;31m";
const string C_Blue   = "\033[01;34m";
const string C_Yellow = "\033[01;33m";

#define doReset (Colors::prettyPrint ? C_Reset : "" )
#define setGreen (Colors::prettyPrint ? C_Green : "" )
#define setRed (Colors::prettyPrint ? C_Red : "" )
#define setBlue (Colors::prettyPrint ? C_Blue : "" )
#define setYellow (Colors::prettyPrint ? C_Yellow : "" )

struct Colors {
    static bool prettyPrint;
};

#endif
