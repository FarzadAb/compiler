#ifndef __ERRORHANDLER__
#define __ERRORHANDLER__ 1
#include <iostream>
#include <map>
#include <vector>
#include <stack>
#include <map>
#include <string>
#include "scanner.hpp"
using namespace std;
#define PB push_back

enum ErrorType{
    ERR_SCAN,
    ERR_PARS,
    ERR_SEM,
    WARN
};

struct ErrorHandler{
    int numErrors;
    Scanner* scanner;
    vector<int> types;
    vector<string> messages;
    ErrorHandler(Scanner* scanner);
    ErrorHandler();
    void inform(ErrorType type,string message);
};


#endif
